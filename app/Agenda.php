<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Alur
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Alur newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Alur newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Alur query()
 * @mixin \Eloquent
 */
class Agenda extends Model
{
    protected $table = 'agenda';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
