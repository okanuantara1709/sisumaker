<?php

namespace App\Http\Controllers;

use App\Pengumuman;
use App\Helpers\Alert;
use App\Helpers\ControllerTrait;
use App\User;
use App\Helpers\AppHelper;
use Illuminate\Http\Request;
use Auth;

class PengumumanController extends Controller
{
    use ControllerTrait;

    private $template = [
        'title' => 'Pengumuman',
        'route' => 'pengumuman',
        'menu' => 'pengumuman',
        'icon' => 'fa fa-cogs',
        'theme' => 'skin-blue',
        'config' => [
            'index.delete.is_show' => false
        ]
    ];

    private function form()
    {
        $status = [
            [
                'value' => 'Aktif',
                'name' => 'Aktif'
            ],
            [
                'value' => 'Tidak Aktif',
                'name' => 'Tidak Aktif'
            ]
        ];
        return [
            [
                'label' => 'Judul',
                'name' => 'judul',
                'view_index' => true
            ],
            [
                'label' => 'Deskripsi',
                'name' => 'deskripsi',
                'view_index' => true
            ],
            [
                'label' => 'Tanggal Berlaku',
                'name' => 'tanggal_berlaku',
                'type' => 'datepicker',
                'view_index' => true
            ],
            [
                'label' => 'Tanggal Kadaluarsa',
                'name' => 'tanggal_kadaluarsa',
                'type' => 'datepicker',
                'view_index' => true
            ],
            [
                'label' => 'File',
                'name' => 'file',
                'type' => 'file',
                'view_index' => false
            ],
            [
                'label' => 'Status',
                'name' => 'status',
                'type' => 'select',
                'option' => $status,
                'view_index' => true
            ],
            [
                'label' => 'Pembuat',
                'name' => 'user_id',
                'type' => 'hidden',
                'value' => Auth::user()->id,
                'hidden' => true,
                'view_index' => true,
                'view_relation' => 'user->nama'
            ],
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pengumuman::all();
        $form = $this->form();
        $template = (object) $this->template;
        return view('admin.master.index',compact('data','form','template'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $template = (object) $this->template;
        $form = $this->form();
        return view('admin.master.create', compact('template','form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->formValidation($request);
        $uploader = AppHelper::uploader($this->form(),$request);
        $pengumuman = $request->all();
        $pengumuman['file'] = $uploader['file'];
        Pengumuman::create($pengumuman);
        Alert::make('success','Berhasil simpan data');
        return redirect(route($this->template['route'].'.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = $this->form();
        $template = (object) $this->template;
        $data = Pengumuman::findOrFail($id);
        return view('admin.master.show',compact('form','template','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = $this->form();
        $template = (object) $this->template;
        $data = Pengumuman::findOrFail($id);
        return view('admin.master.edit',compact('form','template','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->formValidation($request);
        $uploader = AppHelper::uploader($this->form(),$request);
        $data = $request->all();
        $data['file'] = $uploader['file'];
        $pengumuman = Pengumuman::findOrFail($id);
        $pengumuman->update($data);
        Alert::make('success','Berhasil simpan data');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
