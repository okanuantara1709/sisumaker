<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agenda;
use App\Pengumuman;

class DashboardController extends Controller
{
    private $template = [
        'title' => 'Dashboard',
        'route' => 'dashboard',
        'menu' => 'dashboard',
        'icon' => 'fa fa-home',
        'theme' => 'skin-blue'
    ];

    public function index()
    {
        $now = date('Y-m-d');
        $agenda = Agenda::whereDate('tanggal', '>=', "$now")
            ->where('status','Aktif')
            ->get();
        $pengumuman = Pengumuman::whereDate('tanggal_berlaku', '<=' , "$now")
            ->whereDate('tanggal_kadaluarsa', '>=' , "$now")
            ->where('status','Aktif')
            ->get();
        $template = (object) $this->template;
        return view('admin.dashboard.index',compact('template','pengumuman','agenda'));
    }
}
