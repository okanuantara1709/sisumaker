@extends('admin.layouts.app',[$template])
@push('css')

@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$template->title}}
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('admin.dashboard.index')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Home</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                @if(count($pengumuman) > 0)
                <div class="col-md-12">
                    <h3>Pengumuman</h3>
                    @foreach ($pengumuman as $item)
                        <div class="alert alert-info" style="margin-bottom:10px">
                            <h4>{{$item->judul}}</h4>
                            <p>{{$item->deskripsi}}</p>
                        </div>
                    @endforeach
                </div>
                @endif
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Agenda Yang Akan Datang</h3>

                        </div>
                        <box-body> <table class="table table-bordered">
                            <thead>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Judul</th>
                                <th>Deskripsi</th>
                                <th>Lokasi</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                @foreach ($agenda as $key => $item)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$item->tanggal}}</td>
                                    <td>{{$item->judul}}</td>
                                    <td>{{$item->deskripsi}}</td>
                                    <td>{{$item->lokasi}}</td>
                                    <td><a href="{{route('agenda.show',[$item->id])}}" class="btn btn-info btn-sm">Lihat</a></td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table></box-body>

                    </div>

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    </template>
@endsection


